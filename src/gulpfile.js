const $ = require('gulp-load-plugins')();
const gulp = require('gulp');
const fs = require('fs');
const path = require('path');
const args = require('yargs').argv;
const del = require('del');
const globby = require('globby');
const PluginError = $.util.PluginError;
const browserSync = require('browser-sync').create();
const pify = require('pify');
const stat = pify(fs.stat);
const config = require('./gulp.config');

// Mode switches
// Example:
//    gulp --usesourcemaps

const useSourceMaps = args.usesourcemaps || args.usesourcemap;

// production mode (--prod)
const isProduction = !!args.prod;

if (isProduction) log('Running for production...');

// Helps to detect changes in layout or templates
// included from other files and ignore gulp-changed
// (see tasks 'templates:views' and 'watch')
let isLayoutOrTpl = false;

//---------------
// TASKS
//---------------

// JS
const scripts = () =>
    gulp
        .src(config.source.scripts)
        .pipe($.jsvalidate())
        .on('error', handleError)
        .pipe($.concat('app.js'))
        .on('error', handleError)
        .pipe($.if(isProduction, $.uglify(config.uglify)))
        .on('error', handleError)
        .pipe(gulp.dest(config.build.scripts));

// APP Styles
const stylesApp = () => {
    log('Building application styles..');
    return (
        gulp
            .src(config.source.styles.app)
            .pipe($.if(useSourceMaps, $.sourcemaps.init()))
            .pipe($.sass())
            .on('error', handleError)
            .pipe($.clipEmptyFiles())
            //.pipe($.sort())
            .pipe($.concat('app.css'))
            .pipe($.if(isProduction, $.cleanCss()))
            .pipe($.if(useSourceMaps, $.sourcemaps.write()))
            .pipe(gulp.dest(config.build.styles))
            .pipe(browserSync.stream())
    );
};

// Bootstrap to RTL
const stylesBootstrapRTL = () => {
    log('Building bootstrap RTL styles..');
    return gulp
        .src(path.join(config.build.vendor, 'bootstrap/dist/css/bootstrap.min.css'))
        .pipe($.rtlcss()) /* RTL Magic ! */
        .pipe($.rename('bootstrap-rtl.css'))
        .pipe(gulp.dest(config.build.vendor))
        .pipe(browserSync.stream());
};

// APP RTL
const stylesAppRTL = () => {
    log('Building application RTL styles..');
    return gulp
        .src(path.join(config.build.styles, 'app.css'))
        .pipe($.rtlcss()) /* RTL Magic ! */
        .pipe($.rename('app-rtl.css'))
        .pipe(gulp.dest(config.build.styles))
        .pipe(browserSync.stream());
};

// VIEWS
const templatesViews = () => {
    log('Building views.. ');

    const pugFilter = $.filter('**/*.pug', {
        restore: true
    });

    return (
        gulp
            .src(config.source.templates.views)
            .pipe(
                $.if(
                    !isProduction && !isLayoutOrTpl,
                    $.changed(config.build.templates.views, {
                        extension: '.html',
                        hasChanged: fileHasChanged
                    })
                )
            )
            .on('error', handleError)
            .pipe(pugFilter)
            // .pipe(
            //     $.data(() => ({
            //         scripts: globby.sync(config.source.scripts)
            //     }))
            // )
            .pipe($.pug(config.pugOpts))
            .on('error', handleError)
            .pipe(pugFilter.restore)
            .pipe($.flatten())
            // useref is used only to copy files to vendor directory. Minification is done by task 'compress' for production
            .pipe(
                $.useref({
                    noconcat: true,
                    searchPath: '.',
                    transformPath: filePath => filePath.replace('vendor/', 'node_modules/')
                })
            )
            .on('error', handleError)
            .pipe(
                $.tap((file, t) => {
                    if (['.js', '.css'].indexOf(file.extname) >= 0) {
                        file.base = 'node_modules';
                    }
                    return file;
                })
            )
            .pipe($.if('*.css', gulp.dest(config.build.vendor)))
            .pipe($.if('*.js', gulp.dest(config.build.vendor)))
            .pipe($.if('*.html', gulp.dest(config.build.templates.views)))
    );
};

// Compress assets for production
const compress = () => {
    log('Compressing vendor assets.. ');

    const assets = config.build.vendor + '/**/*.{css,js}';
    const jsFilter = $.filter('**/*.js', { restore: true });
    const cssFilter = $.filter('**/*.css', { restore: true });

    return gulp
        .src(assets)
        .pipe(cssFilter)
        .pipe($.cleanCss(config.cleanCss))
        .on('error', handleError)
        .pipe(cssFilter.restore)
        .pipe(jsFilter)
        .pipe($.uglify(config.uglifyVendor))
        .on('error', handleError)
        .pipe(jsFilter.restore)
        .pipe(gulp.dest(config.build.vendor));
};

// Images
const images = () => {
    log('Copying images..');
    return gulp.src(config.source.images).pipe(gulp.dest(config.build.images));
};

// Fonts
const fonts = () => {
    log('Copying fonts..');
    return gulp
        .src(config.source.fonts)
        .pipe($.flatten())
        .pipe(gulp.dest(config.build.fonts));
};

// Copy static assets (json)
const static = () => {
    log('Copying static assets (json)..');
    return gulp.src(config.source.static).pipe(gulp.dest(config.build.static));
};

// Copy vendor assets
const vendor = () => {
    log('Copying vendor assets..');
    return gulp
        .src(config.source.vendor, { base: config.paths.npm })
        .pipe(gulp.dest(config.build.vendor));
};

//---------------
// WATCH
//---------------

// Rerun the task when a file changes
const watch = () => {
    log('Starting Watch..');

    // assets
    gulp.watch(config.source.images, images);
    gulp.watch(config.source.fonts, fonts);
    gulp.watch(config.source.static, static);
    gulp.watch(config.source.vendor, vendor);
    // code
    gulp.watch(config.source.scripts, gulp.series(scripts, reload));
    gulp.watch(config.source.styles.app, gulp.series(stylesApp));
    // Compile templates and reload browsersync
    gulp.watch(config.source.templates.watch, gulp.series(templatesViews, reload)).on(
        'change',
        file => {
            // Each time a pug file changes, check if layout or template
            // if so, activate flag to ignore changed control since we don't have
            // counterpart file to detect file time differences
            const fname = path.basename(file);
            isLayoutOrTpl = false;
            if (fname.indexOf('.layout.') > -1 || fname.indexOf('.tpl.') > -1) isLayoutOrTpl = true;
        }
    );
};

const reload = done => {
    browserSync.reload();
    done();
};

//---------------
// BROWSER SYNC
//---------------

// Rerun the task when a file changes
const browsersync = done => {
    log('Starting BrowserSync..');

    browserSync.init({
        notify: false,
        server: {
            baseDir: '..'
        }
    });

    done();
};

//---------------
// LINT
//---------------

const lint = () => {
    return gulp
        .src(config.source.scripts)
        .pipe($.jshint())
        .pipe(
            $.jshint.reporter('jshint-stylish', {
                verbose: true
            })
        )
        .pipe($.jshint.reporter('fail'));
};

//---------------
// CLEAN
//---------------

// Remove dist folder
const clean = done => {
    const deletePaths = [].concat(config.paths.dist);

    log('Cleaning: ' + $.util.colors.blue(deletePaths));

    // force: clean files outside current directory
    del(deletePaths, {
        force: true
    }).then(() => done());
};

//---------------
// MAIN TASKS
//---------------

// Prepare tasks for build all
let buildTasks = [scripts, stylesApp, templatesViews, images, static, vendor];
if (isProduction) buildTasks.push(compress);

const build = gulp.series(...buildTasks);

// Build source files and assets
exports.build = build;
// Default build and watch (no server)
exports.default = gulp.series(build, watch);
// Server for development
exports.serve = gulp.series(build, browsersync, watch);
// Delete build files
exports.clean = clean;
// Lint javascript
exports.lint = lint;
// Compiles Bootstrap and app styles as RTL
exports.rtl = gulp.series(stylesBootstrapRTL, stylesAppRTL);

// Minifiy vendor assets
exports.compress = compress;

/////////////////////

// Error handler
function handleError(err) {
    log(err.toString());
    this.emit('end');
}

// log to console using
function log(msg) {
    $.util.log($.util.colors.blue(msg));
}

// We are using a different folder structure in src and dist.
// with this function we compare each file time to
// detect what have changed no matter their location
// (Compares one by one -> pug vs html)
function fileHasChanged(stream, sourceFile, destPath) {
    const destPathTo = config.build.templates.views;
    const modDestPath = destPathTo + path.basename(destPath);

    return stat(modDestPath).then(targetStat => {
        if (sourceFile.stat.mtime > targetStat.mtime) {
            stream.push(sourceFile);
        }
    });
}
